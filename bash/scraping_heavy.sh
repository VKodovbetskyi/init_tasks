#!/bin/bash

# Script finds heavy files in user's home dirrectory recurcevely and proposes to delete (with loggin this action) or compress it.

declare -a arr
for item in `find ~ -type f -size +100k`
do
    arr=("${arr[@]}" "$item")
done

for file in ${arr[@]}
    do
    echo "$(basename $file) size is more than 100k: $[ $(stat -c %s $file) / 1024 ]K. Press [D] to delete or [C] to compress one."
    read answer
    if [ $answer="D" ]
    then
        `echo $(date +"%D %T") $file >> ~/deleted_items`
        `rm $file`
    elif [ $answer="C" ]
    then
        `gzip -f $file > $file.gz`
    fi
done