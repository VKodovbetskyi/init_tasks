#!/bin/bash

# The script adds the current date in front of *.jpeg file name of the current dirrectory

curent_date=$(date +%F)

for file in *.jpg
do
    mv $file $curent_date-$file
done