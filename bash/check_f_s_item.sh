#!/bin/bash

# This script checks recieved as a command-line argument path to filesystem item:
# 1 - is file
# 2 - is dirrectory

echo $1
if [ -f $1 ]; then
	echo 1
elif [ -d $1 ]; then
	echo 2
else
	echo wrong argument
fi