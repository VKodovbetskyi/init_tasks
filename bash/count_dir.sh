#!/bin/bash

# This script counts number of files that current working dirrectory contains
# P.S. Including hidden (starting-point) ones.

countDir() {
	echo "$PWD"
	find $PWD -maxdepth 1 -type f ! -path "." | wc -l
}

countDir