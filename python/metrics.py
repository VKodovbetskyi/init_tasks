import socket
import time
import sys
from random import uniform

BACKEND_SERVER = '35.233.28.44'
CARBON_PORT = 2003
STATSD_PORT = 8125
DELAY = 5  # secs
msg_format = sys.argv[1]

def form_carbon_msg():
    timestamp = int(time.time())
    hosts = ["server1", "server2", "server3", "server4", "server5"]
    components = ["cpu", "memory", "disk", "network", "cpu_temperature"]
    message = ""
    for host in hosts:
        for component in components:
            message = message + (f"script_carbon.{host}.{component} {uniform(0, 100):.3f} {timestamp}\n")
    return message

def form_statsd_msg():
    timestamp = int(time.time())
    hosts = ["server1", "server2", "server3", "server4", "server5"]
    components = ["cpu", "memory", "disk", "network", "cpu_temperature"]
    message = ""
    for host in hosts:
        for component in components:
            message = message + (f"script_statsd.{host}.{component}:{uniform(0, 100):.3f}|g\n")
    return message

def sock_conn(server, port, message):
    conn = socket.create_connection((server, port))
    conn.sendall(message.encode("utf-8"))
    conn.close()

def send_msg(msg_format):
    if msg_format == "carbon":
        message = form_carbon_msg()
        print("sending message:\n{}".format(message))
        sock_conn(BACKEND_SERVER, CARBON_PORT, message)
    elif msg_format == "statsd":
        message = form_statsd_msg()
        print("sending message:\n{}".format(message))
        sock_conn(BACKEND_SERVER, STATSD_PORT, message)
    else:
        print(f'Wrong or not set metrics type: "{msg_format}"! Please, provide it to the script in a first argument.')

if __name__ == '__main__':
    while True:
        send_msg(msg_format)
        time.sleep(DELAY)