# This script defines function which counts number of files that current working dirrectory contains
# P.S. Including hidden (starting-point) ones.

import os

print(os.scandir(os.getcwd()))

def count_dir():

	counter = 0

	for item in os.scandir(os.getcwd()):
		if item.is_file():
			counter += 1

	print(counter)

count_dir()