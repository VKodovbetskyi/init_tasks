# This script checks recieved as a command-line argument path to filesystem item:
# 1 - is file
# 2 - is dirrectory

import sys
import os

path = sys.argv[1]

if os.path.isfile(path):
	print(1)
elif os.path.isdir(path):
	print(2)
else:
	print("wrong argument")